var session = require('./session');
var debug = require('debug')('Ditto:Jenny:get_documentation');
var redis = require('redis');
var sanitizer = require('sanitizer');
var config_app = null;
var config_redis = null;
var client = null;

var exports = module.exports = {};


exports.set_user = function(client, config, req, res, next) {
    client = client;
    config_app = config.app;
    config_redis = config.redis;
    var cookies = session.get_cookies(req);
    
    if (cookies.active_user_id != null && cookies.user_token != null && cookies.user_authentication != null) {
        client.hget(config_redis.session.keys.session, cookies.active_user_id + ':' + cookies.user_token, function(err, reply) {
            if (reply != null) {
                try {
                    var user = JSON.parse(reply);
                    debug(user);
                    req.app.set('user', user);
                    next();
                } catch (e) {
                    debug('Exception: ' + e.message);
                    req.app.set('user', null);
                    next();
                }
            } else {
                debug('Content on redis is null');
                req.app.set('user', null);
                next();
            }
        });
    } else {
        debug('invalid parameters of user');
        req.app.set('user', null);
        next();
    }
}