var session = require('./session');
var need_ci = require('./need_ci');
var get_documentation = require('./get_documentation');
var debug = require('debug')('Ditto:Jenny:index');

module.exports = {
    valid_session: function(redis, config, req, res, next) {
        return session.is_valid(redis, config, req, res, next);
    },
    need_ci: function(config, path) {
        return need_ci.find(config, path);
    },
    get_documentation: function(client, config, req, res, next) {
        return get_documentation.set_user(client, config, req, res, next);
    },
    del_session: function(redis, config, res, req) {
        session.del_session(redis, config, res, req);
    },
    get_cookies: function(req) {
        return session.get_cookies(req);
    },
    exists_cookies: function(req) {
        return session.exists_cookies(req);
    },
    generate_session: function(client, config, data, res) {
        debug("generate_session");
        return session.identify(client, config, data, res);
    }
};