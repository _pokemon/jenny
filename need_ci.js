var debug = require('debug')('Ditto:Jenny:need_ci');
var exports = module.exports = {};

exports.find = function(config, path) {
    var response = true;
    var exception_user = config.app.exception_users;
    debug(exception_user);
    Object.keys(exception_user).forEach(function(index) {
        obj = exception_user[index];
        if(obj.path == path || (obj.path + '/') == path){
        	response = false;
        }
    });

    return response;
}