var redis = require('redis');
var moment = require('moment');
var sanitizer = require('sanitizer');
var debug = require('debug')('Ditto:Jenny:session');
var exports = module.exports = {};
var uuid = require('lil-uuid');
var client = null;
var session = null;
var config_app = null;

/**
 * Public method that validate if a session exists for the current user
 * @param  Object   req [Object of expressJs for the controle of resquest]
 * @param  Object   res [Object of expressJs for the controle of response]
 * @param  Function next [Function that controls the flow]
 * @return Void
 */
exports.is_valid = function(client_redis, config, req, res, next) {
    client = client_redis;
    config_app = config.app;
    var app = req.app.get('app');
    var user = req.app.get('user');
    var cookies = get_cookies(req);
    if (!isNaN(cookies.active_user_id) && uuid.isUUID(cookies.user_token) && uuid.isUUID(cookies.user_authentication)) {
        if (user == null || user == undefined || user == '') {
            debug('Status session is null');
            del_session(client, config, res, req);
            res.redirect(config_app.app.redirect.login);
        } else {
            var expired = moment(user.expiration_date, 'YYYY-MM-DD');
            var now = moment();
            if (expired.diff(now, 'hours') > 0 && cookies.user_token == user.token && cookies.user_authentication == user.hash) {
                debug('Connexion is valid');
                next();
            } else {
                debug('The session is expired');
                del_session(client, config, res, req);
                res.redirect(config_app.redirect.access_denied);
            }
        }
    } else {
        debug('invalid parameters of user');
        del_session(res, req);
        res.redirect(config_app.redirect.access_denied);
    }
}

/**
 * Public method that delete the session of an user
 * @param  Object   req [Object of expressJs for the controle of resquest]
 * @param  Object   res [Object of expressJs for the controle of response]
 * @param  Function next [Function that controls the flow]
 * @return Void
 */
exports.del_session = function(client, config, res, req, next) {
    del_session(client, config, res, req);
}

/**
 * Public method that get sessesion from cookies
 * @param  Object   req [Object of expressJs for the controle of resquest]
 * @return Object
 */
exports.get_cookies = function(req) {
    return get_cookies(req);
}

/**
 * Public method that validate if exists cookies and if the format is correct
 * @param  Object   req [Object of expressJs for the controle of resquest]
 * @return Boolean
 */
exports.exists_cookies = function(req) {
    var cookies = get_cookies(req);
    var respose = false;
    if (!isNaN(cookies.active_user_id) && uuid.isUUID(cookies.user_token) && uuid.isUUID(cookies.user_authentication)) {
        respose = true;
    }
    return respose;
}

/**
 * Public method that generate a session for an user
 * @param  Object   user    [Object that content all information for an user]
 * @param  Json     body    [String in json format that content all information for an user]
 * @param  Object   res     [Object of expressJs for the controle of response]
 */
exports.identify = function(client, config, data, res) {
    return identify(client, config, data, res);
}

/**
 * Method that get sessesion from cookies
 * @param  Object   req [Object of expressJs for the controle of resquest]
 * @return Object
 */
function get_cookies(req) {
    try {
        var cookies = {
            active_user_id: sanitizer.escape(req.cookies.charmander),
            user_token: sanitizer.escape(req.cookies.squirtle),
            user_authentication: sanitizer.escape(req.cookies.bulbasaur)
        };
    } catch (e) {
        var cookies = {
            active_user_id: '',
            user_token: '',
            user_authentication: ''
        };
    }
    return cookies;
}

/**
 * Method that generate a session for an user
 * @param  Object   user    [Object that content all information for an user]
 * @param  Json     body    [String in json format that content all information for an user]
 * @param  Object   res     [Object of expressJs for the controle of response]
 * @return Boolean
 */
function identify(client, config, data, res) {
    debug("BEFORE USER IN JENNY",data);
    var user = JSON.parse(data.data);
    debug("USER IN JENNY",user);
    var response = false;
    try {
        res.cookie('charmander', user._id, {
            domain: config.app.domain,
            path: '/',
            secure: config.api.protocol.https ? true : false,
            httpOnly: true
        });

        res.cookie('squirtle', user.token, {
            domain: config.app.domain,
            path: '/',
            secure: config.api.protocol.https ? true : false,
            httpOnly: true
        });

        res.cookie('bulbasaur', user.hash, {
            domain: config.app.domain,
            path: '/',
            secure: config.api.protocol.https ? true : false,
            httpOnly: true
        });
        debug("ok");

        if (client.hset(config.redis.session.keys.session, user._id + ':' + user.token, JSON.stringify(user), redis.print)) {
            response = true;
        }
    } catch (e) {
        debug('[Login][Connect][set_session] ' + e.message);
    }

    return response;
}

/**
 * Method that delete the session of an user
 * @param  Object   req [Object of expressJs for the controle of resquest]
 * @param  Object   res [Object of expressJs for the controle of response]
 * @return Void
 */
function del_session(client, config, res, req) {
    var app = req.app.get('app');
    var cookies = get_cookies(req);

    // Clear cookies
    debug('Clear cookies');

    res.clearCookie('charmander', {
        domain: config_app.domain
    });
    res.clearCookie('squirtle', {
        domain: config_app.domain
    });
    res.clearCookie('bulbasaur', {
        domain: config_app.domain
    });

    // Clear redis key
    debug('Clear redis key');
    client.hdel(config.redis.session.keys.session, cookies.active_user_id + ':' + cookies.user_token);
    client.hdel(config.redis.session.keys.layout, cookies.active_user_id + ':' + cookies.user_token);
}
